##############################################################################
# Project: Bingham Diabetes Mail List
# Date: 6/10/14
# Author: Sam Bussmann
# Description: Do QA on pt_flag and match rates
# Notes: 
##############################################################################

## Investigate Missing Flags
table(cmty_wtgt$ComHHFlag,cmty_wtgt$ComPerFlag,useNA="ifany",dnn=c("ComHHFlag","ComPerFlag"))
table(cmty_wtgt$ComPerFlag,cmty_wtgt$MktPerFlag,cmty_wtgt$ComHHFlag,useNA="ifany",
      dnn=c("ComPerFlag","MktPerFlag","ComHHFlag"))


## Total Pts
a<-dim(pts_unq)[1]
a
## Total Pts matching cmty
b<-sum(cmty_wtgt$CommunityPersonID %in% pts_unq$CommunityPersonID)
b
## Total Pts matching at Person Level
c<-sum(cmty_wtgt$CommunityPersonID %in% pts_unq$CommunityPersonID & cmty_wtgt$ComPerFlag==1)
c
## Total Pts matching at HH Level
d<-sum(cmty_wtgt$CommunityPersonID %in% pts_unq$CommunityPersonID & cmty_wtgt$ComPerFlag==0 
       & cmty_wtgt$ComHHFlag==1)
d
## Match Rate of patients to infogroup database
(c+d)/a

## Investigate low match rate
## Find zips comprising zervice area

library(RODBC)
sql<-odbcConnect("IrmSqls")
zips.insa<-sqlQuery(sql,"SELECT distinct Zipcode 
                    from IRMDm_BinHeaCar68.dbo.starkarea",errors=T,stringsAsFactors = F,as.is=T)

## Percent of patients in target not falling in service area
1-mean((cmty_wtgt$zipcode[cmty_wtgt$pt_flag==1] %in% zips.insa[,1]))
## Sum of all patients not falling in service area
e <- a - sum((cmty_wtgt$zipcode[!is.na(cmty_wtgt$PayerType)] %in% zips.insa[,1]))
## Percent of all patients not falling in service area
e/a

## Match rate among those in service area
(c+d)/(a-e)

### What percent of the non-matches are outside service area
### And what percentage are at an address recognized by infogroup

## Matching Addresses
maddr<-unique(cmty_wtgt$AddressID[cmty_wtgt$ComHHFlag==1])

##Unique Patients considered "non match" in service area
f <- sum((cmty_wtgt$zipcode[!is.na(cmty_wtgt$PayerType) & cmty_wtgt$ComHHFlag!=1] %in% zips.insa[,1]),na.rm=T)
## unique patients considered "non match" that are at an infogroup recognized address
g <- sum(unique(cmty_wtgt$AddressID[cmty_wtgt$ComHHFlag!=1]) %in% maddr)

## Percent of non matches in service area that are at a recognized address
g/f

## Precent of unique patients that are non matches but at a recognized address
g/a

## Exploratory analysis on pt flags
## Create DS ##

########### Why am I getting such a low match rate to infogroup within pt_flag==1 on the cmty_wtgt file?

table(cmty_wtgt$pt_flag, (cmty_wtgt$ComHHFlag==1 & cmty_wtgt$ComPerFlag==1 & cmty_wtgt$MktPerFlag==1), 
      useNA="ifany",dnn=c("Pt Flag","Matches InfoGroup"))

table(cmty_wtgt$pt_flag, (cmty_wtgt$ComHHFlag==1 & cmty_wtgt$ComPerFlag==1), 
      useNA="ifany",dnn=c("Pt Flag","Matches InfoGroup"))

table(cmty_wtgt$pt_flag, (cmty_wtgt$ComHHFlag==1), 
      useNA="ifany",dnn=c("Pt Flag","Matches InfoGroup"))


pm<-cmty_wtgt[(cmty_wtgt$ComHHFlag==1 & !is.na(cmty_wtgt$homeage)),]

#test<-pm[is.na(pm$creditcard_bank),]


table(pm$pt_flag, useNA="ifany",dnn=c("Diabetes"))

save(pm,file="pm.RData")
#load("pm.RData")